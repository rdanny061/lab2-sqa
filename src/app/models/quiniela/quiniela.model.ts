import { Partido } from '../../interfaces/partido';
import { Equipo } from '../equipo/equipo.model';

export class Quiniela {
    private partidos: Array<Partido>;

    constructor() {
        this.partidos = new Array();
    }

    public setPartido(p: Partido) {
        this.partidos.push(p);
    }

    buscarPartido(idPartido: number): Partido {
        for (var i = 0; i < this.partidos.length; i++) {
            let p = this.partidos[i];
            if (p.getID() == idPartido) {
                return p;
            }
        }
        return null;
    }

    public getCantidadPartidos(): number {
        return this.partidos.length;
    }

    setJuegaEnCasa(_s: Equipo, p: number) {
        var partido = this.buscarPartido(p);
        if (partido != null) {
            partido.setEquipoLocal(_s);
        } else {
            throw new Error("No se encontró el partido " + p);
        }
    }

    getJuegaEnCasa(p: number): Equipo {
        var partido = this.buscarPartido(p);
        if (partido != null) {
            return partido.getEquipoLocal();
        } else {
            throw new Error("No se ha puesto el cursor de partido");
        }
    }

    setJuegaFuera(_s: Equipo, p: number) {
        var partido = this.buscarPartido(p);
        if (partido != null) {
            partido.setEquipoVisita(_s);
        } else {
            throw new Error("No se encontró el partido: " + p);
        }
    }

    getJuegaFuera(p: number): Equipo {
        var partido = this.buscarPartido(p);
        if (partido != null) {
            return partido.getEquipoVisita();
        } else {
            throw new Error("No se ha puesto el cursor de partido");
        }
    }

    setPronostico(p: number, golesEquipoLocal: number, golesEquipoVisitante: number): void {
        var partido = this.buscarPartido(p);
        if (partido != null) {
            partido.setGolesLocal(golesEquipoLocal);
            partido.setGolesVisita(golesEquipoVisitante);
        } else {
            throw new Error("No se encontró el partido");
        }
    }

    getPronostico(p: number): Array<number> {
        var partido = this.buscarPartido(p);
        if (partido != null) {
            var r:Array<number> = new Array();
            r.push(partido.getGolesLocal());
            r.push(partido.getGolesVisita());
            return r;
        } else {
            throw new Error("No se encontró el partido");
        }
    }

    setPronosticoArray(golesEquipoLocal: Array<number>, golesEquipoVisitante: Array<number>): void {
        if (this.partidos.length > 0) {
            if (this.partidos.length == golesEquipoLocal.length && this.partidos.length == golesEquipoVisitante.length) {
                for (var i = 0; i < this.partidos.length; i++) {
                    this.partidos[i].setGolesLocal(golesEquipoLocal[i]);
                    this.partidos[i].setGolesVisita(golesEquipoLocal[i]);
                }
            }
            else{
                throw new Error("El número de partidos es distinto del número de pronóticos");   
            }
        } else {
            throw new Error("No hay partidos");
        }
    }
}