import { Quiniela } from './quiniela.model';
import { Substitute, Arg } from '@fluffy-spoon/substitute';
import { Partido } from '../../interfaces/partido';
import { Equipo } from '../equipo/equipo.model';

describe('Quiniela', () => {
  it('should create an instance', () => {
    expect(new Quiniela()).toBeTruthy();
  });
});

describe('Pruebas de integración', () => {
  let partido1;
  let quiniela: Quiniela;
  let equipo1: Equipo;
  let equipo2: Equipo;
  var golesLocal = 8;
  var golesVisita = 4;

  /*
  Antes de cada caso de prueba se inicializan las clases 'Quiniela', 'Equipo' y 'Partido', así como se asignan los valores a utilizar en el partido.
  Esto para asegurarse de que en cada caso de prueba, estos sean renovados.
   */
  beforeEach(() => {
    quiniela = new Quiniela();
    equipo1 = new Equipo();
    equipo2 = new Equipo();
    partido1 = Substitute.for<Partido>();
    partido1.setEquipoLocal(equipo1);
    partido1.setEquipoVisita(equipo2);
    partido1.setGolesLocal(golesLocal);
    partido1.setGolesVisita(golesVisita);
    partido1.setID(1);
    quiniela.setPartido(partido1);
  });

  /*
  Después de cada caso de prueba, este resetea todas las clases en null para evitar conflictos. 
  Seguidamente en el beforeEach serán instanciados de nuevo.
   */
  afterEach(() => {
    quiniela = null;
    equipo1 = null;
    equipo2 = null;
    partido1 = null;
  });
  
  /*
  El caso de prueba verifica la cantidad de goles locales de un equipo, para ver si esta es correcta.
  Se expecta: 8 goles, se obtienen 8 goles.
   */
  it('Goles equipo equipo local', () => {
    partido1.getGolesLocal().returns(golesLocal);
    expect(partido1.getGolesLocal()).toBe(golesLocal);
  });

  /*
  El caso de prueba verifica la función de agregar partidos a la lista en quiniela, verificando que se agregó un segundo partido.
  Se expecta: 2 partidos, se obtienen 2 partidos. 
  */
  it('Agregar nuevo partido a quinela', () => {
    var partido2 = Substitute.for<Partido>();
    quiniela.setPartido(partido2);
    expect(quiniela.getCantidadPartidos()).toBe(2);
  });

  /*
  El caso de prueba verifica la función que retorna qué equipo juega en casa, comparándolo con el equipo que se destinó a jugar en casa.
  Se expecta: el equipo1, se obtiene el equipo1. 
  */
  it('Obtener equipo local', () => {
    partido1.getEquipoLocal().returns(equipo1);
    partido1.getID().returns(1);
    expect(quiniela.getJuegaEnCasa(1)).toBe(equipo1);
  });

  /*
  El caso de prueba verifica la función que retorna el pronóstico de los equipos, en este caso verificando el equipo visita.
  Se expecta: 4 goles, se obtienen 4 goles.
   */
  it('Pronostico equipo visita', () => {
    partido1.getID().returns(1);
    partido1.getGolesLocal().returns(golesLocal);
    partido1.getGolesVisita().returns(golesVisita);
    expect(quiniela.getPronostico(1)[1]).toBe(golesVisita);
  });
});
