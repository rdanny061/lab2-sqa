export class Equipo {
    private id:number;
    private pais:string;
    private confederacion:string;
    private numVecesParticipado:number;
    private numVecesFinal:number;
    private numVecesGanadoMundial:number;
    private numPartidosGanados:number;
    private numPartidosPerdidos:number;
    private numPartidosEmpatados:number;
    private golesFavor:number;
    private golesContra:number;

    constructor(){        
    }

    public setID(id:number){
        this.id = id;
    }

    public setPais(nombre:string){
        this.pais = nombre;
    }

    public setConfederacion(nombre:string){
        this.confederacion = nombre;
    }

    public setNumVecesParticipado(cantidad: number){
        this.numVecesParticipado = cantidad;
    }

    public setnumVecesFinal(cantidad: number){
        this.numVecesFinal = cantidad;
    }

    public setNumVecesGanadoMundial(cantidad: number){
        this.numVecesGanadoMundial = cantidad;
    }

    public setNumPartidosGanados(cantidad: number){
        this.numPartidosGanados = cantidad;
    }

    public setNumPartidosPerdidos(cantidad: number){
        this.numPartidosPerdidos = cantidad;
    }

    public setNumPartidosEmpatados(cantidad: number){
        this.numPartidosEmpatados = cantidad;
    }

    public setGolesFavor(cantidad: number){
        this.golesFavor = cantidad;
    }

    public setGolesContra(cantidad: number){
        this.golesContra = cantidad;
    }

    public getID():number{
        return this.id;
    }

    public getPais():string{
        return this.pais;
    }

    public getConfederacion():string{
        return this.confederacion;
    }

    public getNumVecesParticipado():number{
        return this.numVecesParticipado;
    }

    public getNumVecesFinal():number{
        return this.numVecesFinal;
    }

    public getNumVecesGanadoMundial():number{
        return this.numVecesGanadoMundial;
    }

    public getNumPartidosGanados():number{
        return this.numPartidosGanados;
    }

    public getNumPartidosPerdidos():number{
        return this.numPartidosPerdidos;
    }

    public getNumPartidosEmpatados():number{
        return this.numPartidosEmpatados;
    }

    public getGolesFavor():number{
        return this.golesFavor;
    }

    public getGolesContra():number{
        return this.golesContra;
    }
}