export class Estadio {
    private nombre:string;
    private capacidad:number;
    private ciudad:string;
    private annoInauguracion:number;    

    constructor(){

    }

    public setNombre(nombre:string){
        this.nombre = nombre;
    }

    public setCapacidad(cap:number){
        this.capacidad = cap;
    }

    public setCiudad(nombre:string){
        this.ciudad = nombre;
    }

    public setAnnoInauguracion(n:number){
        this.annoInauguracion = n;
    }

    public getNombre():string{
        return this.nombre;
    }

    public getCapacidad():number{
        return this.capacidad;
    }

    public getCiudad():string{
        return this.ciudad;
    }

    public getAnnoInauguracion():number{
        return this.annoInauguracion;
    }
}