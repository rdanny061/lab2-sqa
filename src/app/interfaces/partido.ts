
import { Estadio } from '../models/estadio/estadio.model';
import { Equipo } from '../models/equipo/equipo.model';

export interface Partido {
    setID(id:number):void;
    setEquipoLocal(id:Equipo):void;
    setEquipoVisita(id:Equipo):void;
    setFecha(fecha:string):void;
    setGolesLocal(cantidad:number):void;
    setGolesVisita(cantidad:number):void;
    setEstadio(estadio:Estadio):void;
    setCantidadPersonasAsistieron(cantidad:number):void;
    getID():number;
    getEquipoLocal():Equipo;
    getEquipoVisita():Equipo;
    getFecha():string;
    getGolesLocal():number;
    getGolesVisita():number;
    getEstadio():Estadio;
    getCantidadPersonasAsistieron():number;
}